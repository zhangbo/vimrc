" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" indent
set autoindent
set smartindent

" appearance
colorscheme Tomorrow 
set guifont=Menlo:h12
syntax on

" tab to space
set expandtab
set shiftwidth=4
set smarttab
set tabstop=4
autocmd FileType make set noexpandtab
autocmd FileType html set tabstop=2 shiftwidth=2

" search
set hlsearch
set incsearch

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" encoding
if has('multi_byte')
    set encoding=utf-8
else
    echoerr 'No multi_byte support'
endif

if has('win32')
    set guifontwide=SimHei:h12
    set nobackup
    set nowritebackup
endif


